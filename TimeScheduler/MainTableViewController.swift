//
//  MainTableViewController.swift
//  TimeScheduler
//
//  Created by Parisa Khan on 10/25/18.
//  Copyright © 2018 Parisa Khan. All rights reserved.
//

import UIKit

class MainTableViewController: UITableViewController {
    
    let defaults = UserDefaults.standard
   
    var options = [" ", "Overview", "Log your day", "View Calendar"]
    
    //forkey: "myAct"
    var act = ["School", "Work", "Sports", "Sleep", "Socialize", "Relax"]
    
    //forkey: "personalAct"
    var personalAct = [String]()
    
    //forKey: "personalTimes"
    var personalTimes = [Double]()
    
    var temp = [String]();
    /*
    struct Times {
        var activity = ""
       var hours = 0
        var propertyListRepresentation : [String:Any?] {
            return ["activity" : activity, "hours" : String(hours)]
        }
    }

    var TimesInfo: [Times] = []
*/
    
    override func viewWillAppear(_ animated: Bool) {
        personalAct = defaults.array(forKey: "personalAct") as! [String]
        personalTimes = defaults.array(forKey: "personalTimes") as! [Double]
        
        self.tableView.reloadData()
    }

    //everything before the screen begins
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UITableViewCell.classForCoder(), forCellReuseIdentifier:"reuseIdentifier");
        //sets titles
        self.title = "Main View"
        
        //configure initial values and set in nsuserdefaults
        defaults.set(act, forKey: "myAct")
        personalAct.append("Sports")
        personalAct.append("Relax")
        personalAct.append("School")
        personalTimes.append(4.0)
        personalTimes.append(2)
        personalTimes.append(12)
        defaults.set(personalAct, forKey: "personalAct")
        defaults.set(personalTimes, forKey: "personalTimes")
    
        
        self.tableView.tableFooterView = UIView()
      /*
        TimesInfo.append(Times(activity: "Soccer", hours: 4))
        TimesInfo.append(Times(activity: "Basketball", hours: 2))
        TimesInfo.append(Times(activity: "Comp sci hw", hours: 10))
        TimesInfo.append(Times(activity: "Hanging out with friends", hours: 10))
 */
        
     //   defaults.set(TimesInfo, forKey: "times")
        
        //cant figure out how to put into ns user default
     //   let propertylistSongs = TimesInfo.map{ $0.propertyListRepresentation }
      //  UserDefaults.standard.set(propertylistSongs, forKey: "songs")


    }
    
    func analyze() -> String {
        var text = ""
    let personalActcount = personalAct.count
        var totalsleep = 0.0
        var totalrelax = 0.0
        var totalclass = 0.0
        var totalwork = 0.0
        var totalsocialize = 0.0
        var totalsports = 0.0
    for index in 0...personalActcount-1 {
        if (personalAct[index] == "Sleep") {
            totalsleep += personalTimes[index]
        }
        if (personalAct[index] == "Relax") {
            totalrelax += personalTimes[index]
        }
        
        if (personalAct[index] == "School") {
            totalclass += personalTimes[index]
        }
        if (personalAct[index] == "Work") {
            totalwork += personalTimes[index]
        }
        if (personalAct[index] == "Socialize") {
            totalsocialize += personalTimes[index]
        }
        if (personalAct[index] == "Sports") {
            totalsports += personalTimes[index]
        }
        }
        //random analysis
        if(totalrelax > totalwork) {
        let x = "You spend " + String(totalrelax - totalwork) + " more hours relaxing than you do working. Maybe it's time to change some habits! "
            text.append(x)
        }
        
        if(totalrelax + totalsocialize > totalwork + totalclass) {
            let x = "You spend " + String(totalrelax - totalwork) + " more hours relaxing and socializing than you do working or are in class. "
            text.append(x)

        }
        
        if(totalsports < 6) {
            let x = "You spend " + String(totalsports) + " hours working out. Maybe you should get moving more. "
            text.append(x)

        }
        
        if((totalrelax + totalsocialize) < 5) {
         let x = "You spend " + String(totalrelax + totalsocialize) + " hours relaxing and socializing. Maybe you should take more breaks. "
            text.append(x)
        }
        
        
        
    return text
    }


    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let personalActcount = personalAct.count
       // let TimesInfocount = TimesInfo.count
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        cell.textLabel?.numberOfLines = 0

        
        
       // cell.textLabel?.text = TimesInfo[indexPath.row].activity
        //adds the little arrow to all the other cells excep for the first one becuase it is a description
        if indexPath.row != 0 {
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = options[indexPath.row]
        }
        
        
        if indexPath.row == 0 {
        var text1 = "Activity Overview: " + "\n"
        text1.append(analyze())
        cell.textLabel?.text = text1
        }
        
        /*
        if indexPath.row == 0 {
            for index in 1...TimesInfocount-1 {
                var hours = String(TimesInfo[index].hours)
                var text = "Activity: " + TimesInfo[index].activity + "  " + "Hours: " + hours + "\n"
                
                text1.append(text)

            }
 */
        /*
        if indexPath.row == 0 {
            for index in 1...personalActcount-1 {
               
 var text = "Activity: " + personalAct[index] + "  " + "Hours: " + String(personalTimes[index]) + "\n"
                
                text1.append(text)
                */
    
 
           // cell.textLabel?.text = text1
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        
        let celltext = cell?.textLabel?.text
        if(celltext == "Overview") {
            self.performSegue(withIdentifier: "OverviewSegue", sender: self)
        }
        
        if(celltext == "Log your day") {
            self.performSegue(withIdentifier: "LogSegue", sender: self)
        }
        
        if(celltext == "View Calendar") {
            self.performSegue(withIdentifier: "CalendarSegue", sender: self)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0) {
        if(indexPath.row == 0) {
            return 200;
        }
        }
        return 75;
    }
  
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
