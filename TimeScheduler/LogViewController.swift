//
//  LogViewController.swift
//  TimeScheduler
//
//  Created by Parisa Khan on 10/25/18.
//  Copyright © 2018 Parisa Khan. All rights reserved.
//

import UIKit

class LogViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    let defaults = UserDefaults.standard
    var selectstart = ""
    //Array of strings used by activityPicker
   // var activityArray = ["Class", "Work", "Sleep", "Socialize", "Relax"];
    var act = [String]()
    var personalTimes = [Double]()
    var personalAct = [String]()
    var currentpicker = ""
    
    
    @IBOutlet weak var SelectActivityPicker: UIPickerView!
    
 
    @IBOutlet weak var SelectStartTime: UIDatePicker!
    
    @IBOutlet weak var SelectEndTime: UIDatePicker!
    
 
    //when we sumbit the button, data is collected from the start picker and the end picker, and the global variable currentpicker is saved into nsuserdefaults (persistant data)
    @IBAction func submitButton(_ sender: Any) {
    
        //configuring formatting of pickers
    var hourtotaltime = 0.0;
    let formatter = DateFormatter()
    let formatter2 = DateFormatter()
    formatter.dateFormat = "HH"
    formatter2.dateFormat = "mm"
        
        //start hour - from picker
       let starthours = formatter.string(from: SelectStartTime.date)
        
        //start min - from picker
        var startmins = formatter2.string(from: SelectStartTime.date)
    
        //end hour - from picker
    let endhours = formatter.string(from: SelectEndTime.date)
    //end min - from picker
        var endmins = formatter2.string(from: SelectEndTime.date)
        
        //unwraps the hour values and then calcs the total amount of hours on the activity
        if let start1 = Int(starthours), let end1 = Int(endhours) {
             hourtotaltime = Double(end1 - start1)
            
        }
        
        //calcs the total mins of the activity and adds it as a decimal to the total time ex: 1.25 hours
        if let start2 = Double(startmins), let end2 = Double(endmins) {
            let x = hourtotaltime + ((end2 - start2)/60)
             hourtotaltime = round(100*x)/100
        }
        
        //stores the activity and stores the total time in nsuserdefaults - can be accessed throughout the app now
        personalTimes = defaults.array(forKey: "personalTimes") as! [Double]
        personalAct = defaults.array(forKey: "personalAct") as! [String]
        
        
       personalTimes.append(hourtotaltime)
        
        //default if nothing is chosen
        if currentpicker == ""
        {
            currentpicker = "School"
        }
        personalAct.append(currentpicker)
        
        defaults.set(personalAct, forKey: "personalAct")
        defaults.set(personalTimes, forKey: "personalTimes")
    
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Log Your Day"
    
     SelectActivityPicker.delegate = self
        act = defaults.array(forKey: "myAct") as! [String]
        
        
        // Do any additional setup after loading the view.
    }
    

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return act[row]
    }
    
    //number of sections
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //Number of rows is equal to the size of the activity array
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return act.count
    }
    
    //stores the value of the current picker to a global value so that when we submit the info the correct activity is added to the picker
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        currentpicker = act[row]
        
    }
}
